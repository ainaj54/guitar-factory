package Guitar;

public class ElectricGuitar extends Guitar {

    public ElectricGuitar(String brand, String model, int manufacturedYear) {
        super(brand, model, manufacturedYear);
        this.hasPickUp = true;

    }

    @Override
    public void makeSound() {
        System.out.println("Twaaaaang");
    }


}
