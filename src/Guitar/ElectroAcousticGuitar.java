package Guitar;

public class ElectroAcousticGuitar extends Guitar {


    public ElectroAcousticGuitar(String brand, String model, int manufacturedYear) {
        super(brand, model, manufacturedYear);
        this.hasPickUp = true;

    }

    @Override
    public void makeSound() {
        System.out.println("DRIIINGTWAAANG");
    }
}
