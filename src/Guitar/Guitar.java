package Guitar;

public abstract class Guitar implements Playable{
    private String brand;
    private String model;
    private int manufacturedYear;
    protected Boolean hasPickUp;

    public Guitar(){

    }
    public Guitar(String brand, String model, int manufacturedYear) {
        this.brand = brand;
        this.model = model;
        this.manufacturedYear = manufacturedYear;
    }

    @Override
    public void makeSound() {
        System.out.println("Generic guitar sound!");
    }

    public String getBrand() {
        return brand;
    }
}
