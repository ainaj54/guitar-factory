package Guitar;

public class AcousticGuitar extends Guitar {


    public AcousticGuitar(String brand, String model, int manufacturedYear) {
        super(brand, model, manufacturedYear);
        this.hasPickUp = false;
    }

    @Override
    public void makeSound() {
        System.out.println("drrriiiinnng");
    }
}
