package scania.aina;


import Guitar.*;
//import Guitar.ElectricGuitar;
//import Guitar.AcousticGuitar;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        Guitar myGuitar = new ElectricGuitar("Electric brand", "model", 1992);
        Guitar acousticGuitar = new AcousticGuitar("acoustic brand","nicemodel", 1962);

        ArrayList<Guitar> myGuitarCollection = new ArrayList<Guitar>();
        myGuitarCollection.add(myGuitar);
        myGuitarCollection.add(acousticGuitar);

        for( Guitar guitar: myGuitarCollection){
            System.out.println("guitar = " + guitar.getClass().getSimpleName());

            guitar.makeSound();
        }
        Amplifier myAmp = new Amplifier("model", "model", 600, true);

        try{myAmp.connectGuitar(acousticGuitar);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }

        myAmp.makeSound();
        myAmp.turnVolumUp();
        myAmp.makeSound();
        myAmp.disconnectGuitar();
        try{myAmp.connectGuitar(myGuitar);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        myAmp.makeSound();
        myAmp.disconnectGuitar();


     }
}
